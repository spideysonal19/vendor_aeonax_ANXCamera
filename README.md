# ANXCamera
## Getting Started :
### Cloning :
- Clone this repo in vendor/aeonax/ANXCamera in your working directory by :
```
git clone -b <branch-name> https://gitlab.com/SonalSingh18/anxcamera vendor/aeonax/ANXCamera
```
### Changes Required :
- You will need [these changes in your device tree.](https://github.com/ArrowOS-Devices/android_device_xiaomi_sm6250-common/commit/1416d5ca4813cdd87e7783201ef1d2ed961238da)
- Done, continue building your ROM as you do normally.
